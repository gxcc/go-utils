package main

import (
	"fmt"
	"log/slog"

	"gitee.com/gxcc/go-utils/utils"
)

func main() {

	slog.Info(utils.PinyinFirstUpperCamel("测试"))

	type Person struct {
		Id    string `json:"id"`
		Name  string `json:"name"`
		Email string `json:"email"`
	}

	var persons = []Person{{
		Id:   "111",
		Name: "张一",
	}, {
		Id:   "222",
		Name: "张二",
	}, {
		Id:   "333",
		Name: "张三",
	}}

	var arr = utils.GetFieldStringArray(persons, "Id")
	fmt.Println(len(arr))
}
