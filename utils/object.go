package utils

import "reflect"

// CopyStruct 拷贝struct中的属性值
func CopyStruct(src, dst any, ignoreFields ...string) {
	srcValue := reflect.ValueOf(src).Elem()
	destValue := reflect.ValueOf(dst).Elem()

	var ignoreMap = make(map[string]bool)
	if ignoreFields != nil && len(ignoreFields) > 0 {
		for _, item := range ignoreFields {
			ignoreMap[item] = true
		}
	}

	for i := 0; i < srcValue.NumField(); i++ {
		value := srcValue.Field(i)
		name := srcValue.Type().Field(i).Name
		if _, ok := ignoreMap[name]; ok {
			continue
		}

		dValue := destValue.FieldByName(name)
		if dValue.IsValid() == false {
			continue
		}
		dValue.Set(value)
	}
}

func GetValueFromMap(data map[string]any, keys ...string) any {
	for _, key := range keys {
		if value, ok := data[key]; ok {
			return value
		}
	}
	return nil
}

func GetStringFromMap(data map[string]string, keys ...string) string {
	for _, key := range keys {
		if value, ok := data[key]; ok {
			return value
		}
	}
	return ""
}

func GetInt64FromMap(data map[string]int64, keys ...string) int64 {
	for _, key := range keys {
		if value, ok := data[key]; ok {
			return value
		}
	}
	return 0
}

func GetIntFromMap(data map[string]int, keys ...string) int {
	for _, key := range keys {
		if value, ok := data[key]; ok {
			return value
		}
	}
	return 0
}
