package utils

import "strings"

type HttpProxySvc struct {
	ProxyServerUrl string `json:"proxyServerUrl"`
}

func (svc *HttpProxySvc) HttpGet(requestUrl string, headers map[string]string) (*HttpResult, error) {
	return doHttpGet(requestUrl, headers, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpPostForm(requestUrl string, headers map[string]string, params map[string]string) (*HttpResult, error) {
	return doHttpPostForm(requestUrl, headers, params, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpPostMultipartForm(requestUrl string,
	headers map[string]string,
	textParams map[string]string,
	fileParams map[string]string) (*HttpResult, error) {
	var uploadFileParams = map[string]UploadFile{}
	for field, filePath := range fileParams {
		var fileName = filePath[strings.LastIndex(filePath, "/")+1:]
		uploadFileParams[field] = UploadFile{
			Name:       fileName,
			FilePath:   filePath,
			FileHeader: nil,
		}
	}
	return doHttpPostMultipartForm(requestUrl, headers, textParams, uploadFileParams, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpPostMultipartForm2(requestUrl string,
	headers map[string]string,
	textParams map[string]string,
	fileParams map[string]UploadFile) (*HttpResult, error) {
	return doHttpPostMultipartForm(requestUrl, headers, textParams, fileParams, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpPost(requestUrl string, headers map[string]string, body []byte) (*HttpResult, error) {
	return doHttpPost(requestUrl, headers, body, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpPut(requestUrl string, headers map[string]string, body []byte) (*HttpResult, error) {
	return doHttpPut(requestUrl, headers, body, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpPatch(requestUrl string, headers map[string]string, body []byte) (*HttpResult, error) {
	return doHttpPatch(requestUrl, headers, body, svc.ProxyServerUrl)
}

func (svc *HttpProxySvc) HttpDelete(requestUrl string, headers map[string]string) (*HttpResult, error) {
	return doHttpDelete(requestUrl, headers, svc.ProxyServerUrl)
}
