package utils

import "time"

const (
	TimeFormatRFC3399 = "2006-01-02T15:04:05Z07:00"

	TimeFormatDateTime            = "2006-01-02 15:04:05"
	TimeFormatDateTimeShort       = "20060102150405"
	TimeFormatDateHourMinute      = "2006-01-02 15:04"
	TimeFormatDateHourMinuteShort = "200601021504"
	TimeFormatDate                = "2006-01-02"
	TimeFormatDateShort           = "20060102"
	TimeFormatYearMonth           = "2006-01"
	TimeFormatYearMonthShort      = "200601"
	TimeFormatYear                = "2006"
	TimeFormatMonth               = "01"
	TimeFormatDay                 = "02"
	TimeFormatTime                = "15:04:05"
	TimeFormatTimeShort           = "150405"
	TimeFormatHourMinute          = "15:04"
	TimeFormatHourMinuteShort     = "1504"
	TimeFormatHour                = "15"
	TimeFormatMinute              = "04"
	TimeFormatSecond              = "05"
)

// GetFirstDayOfCurWeek 获取当本周的第一天（将周一作为每周的第一天)
func GetFirstDayOfCurWeek(weekStartWith int) time.Time {
	return GetFirstDayOfWeek(time.Now(), weekStartWith)
}

// GetFirstDayOfWeek 获取t时间所在周的第一天（将周一作为每周的第一天)
// weekStartWith将周几作为一周的第一天（0：周日，1：周一，2：周二...）
func GetFirstDayOfWeek(t time.Time, weekStartWith int) time.Time {
	var weekday = t.Weekday()
	var firstDayStr = t.AddDate(0, 0, weekStartWith-int(weekday)).Format("20060102")
	firstDay, _ := time.ParseInLocation("20060102", firstDayStr, time.Local)
	return firstDay
}

// GetFirstDayOfCurMonth 获取当本月的第一天（将周一作为每周的第一天)
func GetFirstDayOfCurMonth() time.Time {
	return GetFirstDayOfMonth(time.Now())
}

// GetFirstDayOfMonth 获取t时间所在月的第一天（将周一作为每周的第一天)
func GetFirstDayOfMonth(t time.Time) time.Time {
	firstDay, _ := time.ParseInLocation("200601", t.Format("200601"), time.Local)
	return firstDay

}

// GetFirstDayOfCurYear 获取当本月的第一天（将周一作为每周的第一天)
func GetFirstDayOfCurYear() time.Time {
	return GetFirstDayOfYear(time.Now())
}

// GetFirstDayOfYear 获取t时间所在月的第一天（将周一作为每周的第一天)
func GetFirstDayOfYear(t time.Time) time.Time {
	firstDay, _ := time.ParseInLocation("2006", t.Format("2006"), time.Local)
	return firstDay
}
