package utils

import (
	"strings"

	"github.com/mozillazg/go-pinyin"
)

func PinyinFirstUpperCamel(hans string) string {
	a := pinyin.NewArgs()
	a.Style = pinyin.NORMAL
	var pinyinArr = pinyin.LazyPinyin(hans, a)
	var pinyinStr = ""
	for _, item := range pinyinArr {
		if len(item) > 0 {
			pinyinStr += strings.ToUpper(item[0:1]) + item[1:]
		}
	}
	return pinyinStr
}
