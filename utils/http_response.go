package utils

import (
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

func ReturnJson(c *gin.Context, data any) {
	if strings.Index(c.Query("callback"), "jsonp") == 0 {
		c.JSONP(http.StatusOK, gin.H{
			"status":  "0000",
			"message": "OK",
			"data":    data,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":  "0000",
			"message": "ok",
			"data":    data,
		})
	}
}

func ReturnError(c *gin.Context, status string, message string) {
	if strings.Index(c.Query("callback"), "jsonp") == 0 {
		c.JSONP(http.StatusOK, gin.H{
			"status":  status,
			"message": message,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":  status,
			"message": message,
		})
	}
}

func ReturnErrorData(c *gin.Context, status string, message string, data any) {
	if strings.Index(c.Query("callback"), "jsonp") == 0 {
		c.JSONP(http.StatusOK, gin.H{
			"status":  status,
			"message": message,
			"data":    data,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":  status,
			"message": message,
			"data":    data,
		})
	}
}

// ReturnJson2 返回正常json数据，使用http.status作为状态码
func ReturnJson2(c *gin.Context, data any) {
	if strings.Index(c.Query("callback"), "jsonp") == 0 {
		c.JSONP(http.StatusOK, gin.H{
			"status":  http.StatusOK,
			"message": "OK",
			"data":    data,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":  http.StatusOK,
			"message": "ok",
			"data":    data,
		})
	}
}

// ReturnError2 返回错误json数据，使用http.status作为状态码
func ReturnError2(c *gin.Context, status int, message string) {
	if strings.Index(c.Query("callback"), "jsonp") == 0 {
		c.JSONP(http.StatusOK, gin.H{
			"status":  status,
			"message": message,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"status":  status,
			"message": message,
		})
	}
}
