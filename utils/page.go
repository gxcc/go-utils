package utils

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

type PageInfo struct {
	Page      int64 `json:"page"`
	PageSize  int64 `json:"pageSize"`
	PageCount int64 `json:"pageCount"`
	Total     int64 `json:"total"`
}

func LoadPageInfo(c *gin.Context) PageInfo {
	var page, _ = strconv.ParseInt(c.DefaultQuery("page", "1"), 10, 64)
	var pageSize, _ = strconv.ParseInt(c.DefaultQuery("pageSize", "20"), 10, 64)
	return PageInfo{
		Page:     page,
		PageSize: pageSize,
	}
}

func (pageInfo *PageInfo) ResetByTotal(total int64) {
	if pageInfo.PageSize <= 0 {
		pageInfo.PageSize = 20
	}
	pageInfo.Total = total
	pageInfo.PageCount = (total-1)/pageInfo.PageSize + 1
	if pageInfo.PageCount <= 0 {
		pageInfo.PageCount = 1
	}
}

func (pageInfo *PageInfo) GetLimit() int64 {
	return pageInfo.PageSize

}

func (pageInfo *PageInfo) GetOffset() int64 {
	return (pageInfo.Page - 1) * pageInfo.PageSize
}

type PageList struct {
	PageInfo PageInfo `json:"pageInfo"`
	List     any      `json:"list"`
}
