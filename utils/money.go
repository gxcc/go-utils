package utils

import "strconv"

func ToMoneyStr(amount int64) string {
	return strconv.FormatFloat(float64(amount)/100, 'f', 2, 64)
}
