package utils

import (
	"bytes"
	"encoding/json"
)

func JsonMarshal(v any) ([]byte, error) {
	bf := bytes.NewBuffer([]byte{})
	jsonEncoder := json.NewEncoder(bf)
	jsonEncoder.SetEscapeHTML(false)
	if err := jsonEncoder.Encode(v); err != nil {
		return nil, err
	} else {
		return bf.Bytes(), nil
	}
}
func JsonUnmarshal(data []byte, v any) error {
	return json.Unmarshal(data, v)
}
