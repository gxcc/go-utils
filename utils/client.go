package utils

import (
	"encoding/base64"
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
)

type ClientInfo struct {
	AppVersion  string `json:"appVersion"`
	SdkVersion  string `json:"sdkVersion"`
	DeviceType  string `json:"deviceType"` // Android/iOS
	DeviceBrand string `json:"deviceBrand"`
	DeviceModel string `json:"deviceModel"`
	PhoneNumber string `json:"phoneNumber"`
	IMEI        string `json:"imei"`
	IMSI        string `json:"imsi"`
}

func LoadClientInfo(c *gin.Context) ClientInfo {
	var clientInfoEncoded = c.GetHeader("Client-Info-Encoded")
	if len(clientInfoEncoded) == 0 {
		clientInfoEncoded = c.Query("clientInfoEncoded")
	}

	var clientInfo ClientInfo
	if len(clientInfoEncoded) > 0 {
		decodeData, err := base64.StdEncoding.DecodeString(clientInfoEncoded)
		if err != nil {
			fmt.Println("解码clientInfoEncoded出错，错误信息：" + err.Error())
			return ClientInfo{}
		}
		if err := json.Unmarshal(decodeData, &clientInfo); err != nil {
			fmt.Println("解析clientInfoEncoded出错，错误信息：" + err.Error())
			return ClientInfo{}
		}
	}

	return clientInfo
}
