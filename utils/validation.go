package utils

import "regexp"

func IsPhoneNumber(phone string) bool {
	regular := "^(\\+)?\\d{6,15}$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(phone)
}

func IsEmailAddress(email string) bool {
	regular := "^[a-zA-Z0-9]+([._\\-]*[a-zA-Z0-9])*@([a-zA-Z0-9]+[-a-zA-Z0-9]*[a-zA-Z0-9]+.){1,63}[a-zA-Z0-9]+$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(email)
}

func IsStdTimeZone(timezone string) bool {
	regular := "^[\\+\\-][0-9]{2}[:][0-9]{2}$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(timezone)
}

func IsNumber(number string) bool {
	regular := "^-?[0-9]\\d*$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(number)
}
