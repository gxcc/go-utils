package utils

import (
	"math"
	"strconv"
)

// EarthDistance 计算距离
func EarthDistance(lat1, lng1, lat2, lng2 float64) float64 {
	earthRadius := 6378137.0
	rad := math.Pi / 180.0

	radLat1 := lat1 * rad
	radLng1 := lng1 * rad
	radLat2 := lat2 * rad
	radLng2 := lng2 * rad

	a := radLat1 - radLat2
	b := radLng1 - radLng2

	s := 2 * math.Asin(math.Sqrt(math.Pow(math.Sin(a/2), 2)+math.Cos(radLat1)*math.Cos(radLat2)*math.Pow(math.Sin(b/2), 2)))
	return s * earthRadius
}

// ConvertHumanDistance 计算距离
func ConvertHumanDistance(distance float64) string {
	var distanceStr = ""
	if distance > 10000 {
		distanceStr = strconv.FormatFloat(distance/1000, 'f', 0, 64) + "公里"
	} else if distance > 1000 {
		distanceStr = strconv.FormatFloat(distance/1000, 'f', 1, 64) + "公里"
	} else {
		distanceStr = strconv.FormatFloat(distance, 'f', 0, 64) + "米"
	}

	return distanceStr
}
